And('pesquiso caso de teste {string}') do |casoTeste|
    casoTestePage.preenhcerCampoPesquisa(casoTeste)
  end
  
  Then('valido caso de teste retornado') do
    expect(casoTestePage.retornaCasoDeTeste).to eql 'Cadastrar Release'
  end
  
  