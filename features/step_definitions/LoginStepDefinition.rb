Given('informo o usuario {string}') do |usuario|
    loginPage.load
    loginPage.preenhcerUsuario(usuario)
  end
  
  And('informo a senha {string}') do |senha|
    loginPage.preencherSenha(senha)
  end
  
  When('acessar o sistema') do
    loginPage.clicarEmLogin
  end
  
  Then('a página deve ser autenticada') do
    expect(mainPage.retornaTexoInfoDeLogin).to eql 'Bem-vindo ao Crowdtest! O que deseja fazer hoje?'
  end
  
  And('solicito o logout do sitema') do
    mainPage.clicarEmGerenciarButton
    gerenciarProjetosPage.sairDoCrowd
end

Then('o logout deve ser feito com sucesso') do
  expect(loginPage.retornaLoginText).to eql 'Login'
end

Then('a mensagem de senha inválida deve ser mostrada') do
  expect(loginPage.retornaMensagemDeErroSenha).to eql 'E-mail ou senha inválidos.'
  end

Then('a mensagem de email inválido deve ser mostrada') do
    expect(loginPage.retornaMensagemDeErroEmail).to eql 'EMAIL inválido'
    end


  