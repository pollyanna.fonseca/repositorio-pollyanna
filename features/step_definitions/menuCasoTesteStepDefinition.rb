When('solicito acesso ao menu gerenciar projetos') do
    mainPage.clicarEmGerenciarButton
  end
  
  And('solicito acesso ao menu caso de testes') do 
    menuProjetosPage.clicarEmMenuProjetosButton
    menuProjetosPage.clicarEmProjetoButton
    menuProjetosPage.clicarEmMenuCasoTesteButton
  end
      
  Then('sistema verifica caso de teste') do
    expect(casoTestePage.retornaNameDoCasoDeTeste).to eql 'Mensagem (P2A)'
  end

