
Feature: Login

 @login 
Scenario: Efetuar login com sucesso
    Given informo o usuario 'pollyanna.fonseca@base2.com.br'
    And informo a senha 'Polly121225'
    When acessar o sistema
    Then a página deve ser autenticada


Scenario: Efetuar logout com sucesso
    Given informo o usuario 'pollyanna.fonseca@base2.com.br'
    And informo a senha 'Polly121225'
    When acessar o sistema
    And solicito o logout do sitema
    Then o logout deve ser feito com sucesso
 
Scenario: Efetuar login com senha inválida
    Given informo o usuario 'pollyanna.fonseca@base2.com.br'
    And informo a senha '89624'
    When acessar o sistema
    Then a mensagem de senha inválida deve ser mostrada

 
 Scenario Outline: Efetuar login com email inválidos
    Given informo o usuario '<usuario>'
    And informo a senha '<senha>'
    When acessar o sistema
    Then a mensagem de email inválido deve ser mostrada

Examples:
   | usuario | senha |
   |anhjojjo.com|12354|
   |pollyanna.fonseca@email|896144796|
   |carlaaaaaa@email,com|1456984|
   |annnaaaa|21478965|
   |pollyanna.fonseca|89665477|

