Dir[File.join(File.dirname(__FILE__),'../Pages/*Pages.rb')].each { |file| require file}

module PageObjects
    def loginPage
        @loginPage ||= LoginPage.new
    end

    def mainPage
        @mainPage ||= MainPage.new
    end

    def gerenciarProjetosPage
        @gerenciarProjetosPage ||= GerenciarProjetosPage.new
    end

    def menuProjetosPage
        @menuProjetosPage ||= MenuProjetosPage.new
    end

    def casoTestePage
        @casoTestePage ||= CasoTestePage.new
    end
    
end 