class CasoTestePage < SitePrism::Page 

     #Mapping

    element :nameCasoTesteTextArea,:xpath,'//mat-cell[text()="Mensagem (P2A)"]'
    element :pesquisaField,'#testCaseSearch'
    element :casoTesteTextArea,:xpath,'//mat-cell[text()="Cadastrar Release"]'

    #Actions

    def preenhcerCampoPesquisa(casoTeste)
        pesquisaField.set casoTeste
    end

    def retornaCasoDeTeste
        casoTesteTextArea.text
    end 
   
    def retornaNameDoCasoDeTeste
    nameCasoTesteTextArea.text
    end

end  
