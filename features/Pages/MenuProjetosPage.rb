class MenuProjetosPage < SitePrism::Page
    
    #Mapping
    element :menuCasoTesteButton,'#mat-tab-label-0-2'
    element :menuProjetosButton,:xpath,'//a[contains(@href,"/projects")]'
    element :projetoButton,'mat-cell[class="mat-cell cdk-column-name mat-column-name ng-star-inserted"]'
    
    
    #Actions
    def clicarEmMenuProjetosButton
        menuProjetosButton.click
    end

    def clicarEmProjetoButton
        projetoButton.click
    end

   def clicarEmMenuCasoTesteButton
        menuCasoTesteButton.click
   end   
        
end

 
  
  