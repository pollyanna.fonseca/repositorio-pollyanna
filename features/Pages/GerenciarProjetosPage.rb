class GerenciarProjetosPage < SitePrism::Page

      #Mapping
    element :menuProjetosLink,:xpath,'//a[contains(@href,"/projects")]'
    element :logoutButton,'li[class = "li-exit"]'


   #Actions
   def sairDoCrowd
    logoutButton.click
   end

    def preencherCampoPesquisa(issueID)
        searchField.set issueID
    end

    def clicarEmMenuProjetos
        menuProjetosLink.click
    end
    
end
 
