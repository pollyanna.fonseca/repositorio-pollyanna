class MainPage < SitePrism::Page

    #Mapping
    element :gerenciarButton,:xpath,'//button[text()="Gerenciar"]'
    element :loginInfoTextArea,:xpath,'//span[text()="Bem-vindo ao Crowdtest! O que deseja fazer hoje?"]'
   
         
    #Actions
    def retornaTexoInfoDeLogin
        loginInfoTextArea.text
    end

    def clicarEmGerenciarButton
        gerenciarButton.click
    end   
   
end