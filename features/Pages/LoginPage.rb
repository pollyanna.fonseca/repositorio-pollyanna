class LoginPage < SitePrism::Page
    set_url '/auth'

    #Mapping
    element :usernameField,'#login'
    element :passwordField,'#password'
    element :loginButton,'button[class="btn btn-crowdtest btn-block"]'
    element :mensagemErroSenhaTextArea,'span[class="error-msg"]'
    element :loginTextArea,:xpath,'//p[contains(text(),"Login")]'
    element :mensagemErroEmailTextArea,'#elementErrorMsg_login'
   
    #Actions
    def preenhcerUsuario(usuario)
        usernameField.set usuario
    end

    def preencherSenha(senha)
        passwordField.set senha
    end

   def clicarEmLogin
       loginButton.click
   end

    def retornaMensagemDeErroSenha
        mensagemErroSenhaTextArea.text
    end

    def retornaMensagemDeErroEmail
        mensagemErroEmailTextArea.text
    end

    def retornaLoginText
        loginTextArea.text
    end
        
    
end